@extends('layout.user.master')
@section('content')
	<!-- Home -->
	<!-- ****** Welcome Area Start ****** -->
    <section class="caviar-hero-area" id="home">
        <!-- Welcome Social Info -->
        <div class="welcome-social-info">
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        </div>
        <!-- Welcome Slides -->
        <div class="caviar-hero-slides owl-carousel">
            <div class="single-hero-slides bg-img" style="background-image: url(img/bg-img/hero-1.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-11 col-md-6 col-lg-4">
                            <div class="hero-content">
                                <h2>Nhà hàng Lẩu Nấm Chay An Nhiên</h2>
                                <p>Nhà hàng Lẩu Nấm Chay An Nhiên - Hãy một lần ghé về An Nhiên, các thực khách sẽ lạc bước vào một thế giới khác – thế giới của cảm xúc đang có ưu đãi hấp dẫn khi đặt bàn qua PasGo – Tổng đài (+84) 19006005. Xem ngay!</p>
                                <a href="#" class="btn caviar-btn"><span></span> Xem ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Slider Nav -->
                <div class="hero-slides-nav bg-img" style="background-image: url(img/bg-img/hero-2.jpg);"></div>
            </div>
            <!-- Single Slides -->
            <div class="single-hero-slides bg-img" style="background-image: url(img/bg-img/hero-2.jpg);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-11 col-md-6 col-lg-4">
                            <div class="hero-content">
                                <h2>Nhà hàng Lẩu Nấm Chay An Nhiên</h2>
                                <p>Nhà hàng Lẩu Nấm Chay An Nhiên - Hãy một lần ghé về An Nhiên, các thực khách sẽ lạc bước vào một thế giới khác – thế giới của cảm xúc đang có ưu đãi hấp dẫn khi đặt bàn qua PasGo – Tổng đài (+84) 19006005. Xem ngay!</p>
                                <a href="#" class="btn caviar-btn"><span></span> Xem ngay</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Slider Nav -->
                <div class="hero-slides-nav bg-img" style="background-image: url(img/bg-img/hero-1.jpg);"></div>
            </div>
        </div>
    </section>

	<!-- Search -->

	<div class="search">
		
		<!-- Search Contents -->
		
		<div class="container fill_height">
			<div class="row fill_height">
				<div class="col fill_height">

					<!-- Search Tabs -->

					<div class="search_tabs_container">
						<div class="search_tabs d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
							<div class="search_tab active d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src="images/suitcase.png" alt="">Tìm quán theo địa điểm</div>
							<!--<div class="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src="images/bus.png" alt="">tìm quán theo dánh giá</div>
							<div class="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src="images/bus.png" alt="">tìm món ăn</div> -->
							<div class="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src="images/cruise.png" alt="">tìm trên bản đồ</div>
						</div>		
					</div>
					<!-- Search Panel -->
					<div class="search_panel active">
						<form action="{{url('timkiem')}}" method="post" id="search_form_1" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
							<div class="search_item">
								@csrf
								<div>tỉnh</div>
								<select name="province" id="province_1" class="dropdown_item_select search_input" required>
									<option disabled selected value>Chọn địa điểm</option>
									@foreach($provinces as $province)
									<option value="{{$province->id}}">{{$province->_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="search_item">
								<div>quận/huyện</div>
								<select name="district" id="district_1" class="dropdown_item_select search_input">
									
								</select>
							</div>
							<div class="search_item">
								<div>xã/phường</div>
								<select name="ward" id="ward_1" class="dropdown_item_select search_input">
								
								</select>
							</div>
							<div class="search_item">
								<div>đường/phố</div>
								<select name="street" id="street_1" class="dropdown_item_select search_input">
									
								</select>
							</div>
							<button class="button search_button">Tìm<span></span><span></span><span></span></button>
						</form>
					</div>
					
					<!-- Search Panel -->

					<div class="search_panel">
						<form action="{{url('timkiemmap')}}"method="post" id="search_form_2" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
							<div class="search_item"></div>
							<div class="search_item"></div>
							<div class="search_item">
								@csrf
								<div>Khu vực</div>
								<select name="province" id="province_1" class="dropdown_item_select search_input" required>
									<option disabled selected value>Chọn địa điểm</option>
									@foreach($provinces as $province)
									<option value="{{$province->id}}">{{$province->_name}}</option>
									@endforeach
								</select>
							</div>
							<button class="button search_button findmap">Tìm<span></span><span></span><span></span></button>
						</form>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Quan An -->
	
	<div class="intro">
		<div class="container">
			<!-- <div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="intro_text text-center">
						<p>Vì dữ liệu hiện tại của hệ thống không nhiều. Mong các bạn xem trước những quán sau. <b>Song Long</b> sẽ gửi tới các bạn nhiều sự lựa chọn hơn sau khi cập nhật thêm dữ liệu</p>
					</div>
				</div>
			</div> -->
			<div class="row intro_items">
				<!-- Quan An Item -->
			@foreach($quanans as $quanan)
				<div class="col-lg-4 intro_col">
					<div class="intro_item">
						<div class="intro_item_overlay"></div>
						<!-- Image by https://unsplash.com/@dnevozhai -->
						<div class="intro_item_background" style="background-image:url({{$quanan->image}})"></div>
						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
							<div class="intro_date">{{$quanan->created_at}}</div>
							<div class="button intro_button"><div class="button_bcg"></div><a href="{{action('U_homepageController@ChitietQuanan', $quanan->id)}}">Chi tiết<span></span><span></span><span></span></a></div>
							<div class="intro_center text-center">
								<h1>{{$quanan->name}}</h1>
								<div class="intro_price">{{$quanan->soLike}} thích</div>
								<div class="rating rating_4">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
				</div>
			</div>
		</div>

	<!-- Mon An -->

@endsection