@extends('layout.user.master')
@section('content')
<!-- Home -->
<link rel="stylesheet" type="text/css" href="{{asset('lib/styles/timkiem_styles.css')}}">

<div class="home_timkiem">
	<div class="home_background" data-parallax="scroll" style="background-image:url(../images/timkiem_background.jpg)"></div>
	<div class="home_content">
		<div class="home_title"></div>
	</div>
</div>

<!-- Search -->

<div class="search">
	
	<!-- Search Contents -->

	<div class="container fill_height">
		<div class="row fill_height">
			<div class="col fill_height">

				<!-- Search Tabs -->

				<div class="search_tabs_container">
					<div class="search_tabs d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
						<div class="search_tab active d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src="images/suitcase.png" alt=""><span>Tìm quán theo địa điểm</span></div>
						<div class="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src="images/cruise.png" alt="">tìm trên bản đồ</div>
					</div>		
				</div>
				<!-- Search Panel -->
				<div class="search_panel active">
					<form action="{{url('timkiem')}}" method="post" id="search_form_1" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
						<div class="search_item">
							@csrf
							<div>tỉnh</div>
							<select name="province" id="province_1" class="dropdown_item_select search_input" ">
								<option disabled selected value>Chọn địa điểm</option>
								@foreach($provinces as $province)
								<option value="{{$province->id}}">{{$province->_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="search_item">
							<div>quận/huyện</div>
							<select name="district" id="district_1" class="dropdown_item_select search_input">
									<!-- @foreach($districts as $district)
									<option value="{{$district->id}}">{{$district->_name}}</option>
									@endforeach -->
								</select>
							</div>
							<div class="search_item">
								<div>xã/phường</div>
								<select name="ward" id="ward_1" class="dropdown_item_select search_input">
									<!-- @foreach($wards as $ward)
									<option value="{{$ward->id}}">{{$ward->_name}}</option>
									@endforeach -->
								</select>
							</div>
							<div class="search_item">
								<div>đường/phố</div>
								<select name="street" id="street_1" class="dropdown_item_select search_input">
									<!-- @foreach($streets as $street)
									<option value="{{$street->id}}">{{$street->_name}}</option>
									@endforeach -->
								</select>
							</div>
							<button class="button search_button">Tìm<span></span><span></span><span></span></button>
						</form>
					</div>
					
					<!-- Search Panel -->

					<div class="search_panel">
						<form action="{{url('timkiemmap')}}"method="post" id="search_form_2" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
							<div class="search_item"></div>
							<div class="search_item"></div>
							<div class="search_item">
								@csrf
								<div>Khu vực</div>
								<select name="province" id="province_1" class="dropdown_item_select search_input" required>
									<option disabled selected value>Chọn địa điểm</option>
									@foreach($provinces as $province)
									<option value="{{$province->id}}">{{$province->_name}}</option>
									@endforeach
								</select>
							</div>
							<button class="button search_button findmap">Tìm<span></span><span></span><span></span></button>
						</form>
					</div>
				</div>
			</div>
		</div>		
	</div>

	<!-- Offers  -->
	<div style="min-height: 500px">
		<div class="col-lg-12">
			<!-- Offers Grid -->
			<div class="offers_grid">
				<!-- Offers Item -->
				@if(isset($message))
				<div class="notfound">Không tìm thấy quán chay nào ở địa chỉ này </div>
				@elseif(isset($quanans) == null)
				<?php $i=0 ?>
				@foreach($addresses as $address)
				@if($address->quanans->state == 1)		
				<?php $i++ ?>
				@endif
				@endforeach
				<div class="offers_text" style="margin-bottom: 18px;margin-top: 15px;margin-left: 110px;font-size:20px"><b>Có {{$i}} quán ăn được tìm thấy</b> </div>
				@foreach($addresses as $address)
				@if($address->quanans->state == 1)

				<div  class="offers_item rating_4">
					<div class="row">
						<div class="col-lg-1 "><div style="font-size: 40px;margin-top: 57px;margin-left: 33px;color: #909292;"></div></div>
						<div class="col-lg-3 col-1680-4">
							<div class="offers_image_container">
								@if($address->quanans->image_local)
								<div class="offers_image_background" style="background-image:url({{asset('images/quanan/'.$address->quanans->image_local)}})"></div>
								@else 
								<div class="offers_image_background" style="background-image:url({{asset($address->quanans->image)}})"></div>
								@endif
								
								<div class="offer_name"><a href="{{action('U_homepageController@ChitietQuanan',  $address->quanans->id)}}">Đang mở cửa</a></div>
							</div>
						</div>
						<div class="col-lg-8">
							<div class="offers_content">
								<div class="offers_price">{{$address->quanans->name}}</div>
								<p class="offers_text"> {{$address->quanans->diachi}} </p>
								<p class="offers_text">Giá : {{$address->quanans->price}}
									<p class="offers_text">Giờ hoặt động : {{$address->quanans->worktime}} (VND)
										<p class="time_text"><span></span>Đang hoặt động</p>
										
										
										<a href="{{action('U_homepageController@ChitietQuanan', $address->quanans->id)}}">Xem chi tiết</a>
										<div class="offer_reviews">
											@if($address->rates != null)
											<div class="offer_reviews_content">
												<div class="offer_reviews_title">Tổng Đánh giá</div>
												<div class="offer_reviews_subtitle">{{$address->rates->total_rate}} đánh giá & nhận xét</div>
											</div>
											<div class="offer_reviews_rating text-center">{{$address->rates->average*2}}</div>
											@else 
											<div class="offer_reviews_rating text-center">New</div>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
						@endif
						@endforeach

						@else
						<?php $i=0 ?>
						@foreach($quanans as $quanan)
						@if($quanan->state == 1)		
						<?php $i++ ?>
						@endif
						@endforeach
						<div class="offers_text" style="margin-bottom: 18px;margin-top: 15px;margin-left: 110px;font-size:20px"><b>Có {{$i}} quán ăn được tìm thấy</b> </div>
						@foreach($quanans as $quanan)
						@if($quanan->state == 1)

						<div  class="offers_item rating_4">
							<div class="row">
								<div class="col-lg-1 "><div style="font-size: 40px;margin-top: 57px;margin-left: 33px;color: #909292;"></div></div>
								<div class="col-lg-3 col-1680-4">
									<div class="offers_image_container">
										@if($quanan->image_local)
										<div class="offers_image_background" style="background-image:url({{asset('images/quanan/'.$quanan->image_local)}})"></div>
										@else 
										<div class="offers_image_background" style="background-image:url({{asset($quanan->image)}})"></div>
										@endif
										<div class="offer_name"><a href="{{action('U_homepageController@ChitietQuanan', $quanan->id)}}">Đang mở cửa</a></div>
									</div>
								</div>
								<div class="col-lg-8">
									<div class="offers_content">
										<div class="offers_price">{{$quanan->name}}</div>
										<p class="offers_text"> {{$quanan->diachi}} </p>
										<p class="offers_text">Giá : {{$quanan->price}}
											<p class="offers_text">Giờ hoặt động : {{$quanan->worktime}} (VND)
												<p class="time_text"><span></span>Đang hoặt động</p>


												<a href="{{action('U_homepageController@ChitietQuanan', $quanan->id)}}">Xem chi tiết</a>
												<div class="offer_reviews">
													@if($quanan->rates != null)
													<div class="offer_reviews_content">
														<div class="offer_reviews_title">Tổng Đánh giá</div>
														<div class="offer_reviews_subtitle">{{$quanan->rates->total_rate}} đánh giá & nhận xét</div>
													</div>
													<div class="offer_reviews_rating text-center">{{$quanan->rates->average*2}}</div>
													@else 
													<div class="offer_reviews_rating text-center">New</div>
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
								@endif
								@endforeach
								@endif
								<div class="text-center">
								</div>

							</div>
						</div>
					</div>


					@endsection