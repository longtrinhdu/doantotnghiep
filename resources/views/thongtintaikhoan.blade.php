@extends('layout/admin/master')
@section('content')
<div class="container" style="min-height: 500px; margin-top: 50px; margin-bottom: 50px">
<!-- <div class="container" style="background-image:url(images/about_background.jpg)"> -->
@if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
<body id="thongtin">
	<div class="row">
		<div class="col-md-3" style="min-height: 500px; border: 2px solid #a0d09a;margin-right:10px">
			<div class="row">
				<center>
					<img src="../images/avatar.png">					

				</center>
				<div style="margin: 15px">
					<p>Tên: {{Session('dangnhap')->name}}</p>
					<p>Email: {{Session('dangnhap')->email}}</p>
					<p>CMND: {{Session('dangnhap')->cmnd}}</p>

				</div>
				
				
				
			</div>
		</div>
		<div class="col-sm-1"></div>
		<div class="col-md-6" style="min-height: 500px; border: 2px solid #a0d09a;">
			<form class="form-signin" action="{{url('profile')}}" method="post" >
              <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
      <div class="form-label-group">
			<p></p>
      	<h4>Thông tin tài khoản</h4><br>
        <label for="inputName">Tên Tài Khoản</label>
        <input type="name" id="inputName" class="form-control" value="{{Session('dangnhap')->name}}"  name="name" required=""> 
      </div>
      <div class="form-label-group">
      	    <label class=""for="inputEmail">Email</label>
        <input type="email" id="inputEmail" class="form-control" value="{{Session('dangnhap')->email}}" name="email" readonly="readonly" required="" autofocus="">
    
      </div>

      <div class="form-label-group">
      	  <label class=""for="inputPassword">CMND</label>
        <input type="CMND" id="inputPassword" class="form-control" value="{{Session('dangnhap')->cmnd}}" name="cmnd" required="">
      
      </div>
			
      <br>
      <div class="row">
      	<div class="col-md-3" style="float: right;">
      		<button class="btn btn-lg btn-primary btn-block" type="submit">Cập Nhật</button>
      	</div>
      </div>
      
    </form>
		</div>
		
	</div>
</div>
</div>
</div>
</div>
</body>

@endsection