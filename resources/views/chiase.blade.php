@extends('layout.user.master')
@section('content')

    <!-- ***** Breadcumb Area Start ***** -->
    <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/hero-4.jpg)">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content">
                        <h2>Liên hệ</h2>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+1234567890">+84 1642 8989 57</a> 
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 484 Lê Văn Việt, phường Tăng Nhơn Phú A, Quận 9</p>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:longtrinhdu@gmail.com">longtrinhdu@gmail.com</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <!-- ***** Contact Area Start ***** -->
    <div class="caviar-contact-area d-md-flex align-items-center" id="contact">
        <div class="contact-form-area d-flex justify-content-end">
            <div class="contact-form">
                <div id="title_contact" class="contact-form-title">
                    <p>Thông tin địa chỉ trên bản đồ</p>
                </div>
                <div id="form-messages" class="contact_title text-center">a</div>
                <div  id="contact_div">
                    <form  method="post" id="contact_form" class="contact_form text-center" action="quanan/add">
                        <div class="row">
                            <div class="col-12">
                                <input type="text" id="name"  name="tenquanan" class="contact_form_name input_field" placeholder="Nhập vào tên quán ăn" data-error="Name is required." required>
                            </div>
                            <div style="margin-top: 150px;"></div>
                            <div class="col-12">
                                <button type="submit" class="btn caviar-btn"><span></span> Tiếp theo</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div  id="address_div">
                    <form style="display: none;" method="post" id="address_form" class="contact_form text-center" action="quanan/add-address">
                    <div class="row">
                       
                        <div class="col-12">
                            <input type="text" class="form-control" name="lat" id="lat" placeholder="Lat">
                        </div>
                        <div class="col-12">
                            <input type="email" class="form-control"name="lng" id="lng"  placeholder="Lng">
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn caviar-btn"><span></span> Hoàn thành</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        
        <div class="caviar-map-area wow fadeInRightBig" data-wow-delay="0.5s">
            <div id="googleMap"></div>
        </div>
    </div>
  <!-- Google Maps js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzbS2nerrvbdvNlsyQO9cDWFNthdoswRU"></script>    
    <script >
        var map;
        var latlng = new google.maps.LatLng(10.8231, 106.63);
        var stylez = [{
            featureType: "all",
            elementType: "all",
            stylers: [{
                saturation: -100
                    }]
                }];
        var mapOptions = {
            zoom: 15,
            center: latlng,
            disableDefaultUI: true,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'gMap']
            }
        };
         var infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

           var marker = new google.maps.Marker({
                    map: map,
                    draggable:true,
                    icon: 'img/core-img/point.png',
                    position: map.getCenter()
                }); 

        
          google.maps.event.addListener(marker,'click',function(){
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            $('#lat').val(lat);
            $('#lng').val(lng);
          });
            </script>

@endsection