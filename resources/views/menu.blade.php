@extends('layout.user.master')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('lib/styles/single_listing_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('lib/styles/single_listing_responsive.css')}}">
<!-- Recommend -->
<link rel="stylesheet" href="{{asset('lib/plugins/recommend/icomoon.css')}}">
<!-- Owl Carousel -->
<link rel="stylesheet" href="{{asset('lib/plugins/recommend/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('lib/plugins/recommend/owl.theme.default.min.css')}}">
<!-- Theme style  -->
<link rel="stylesheet" href="{{asset('lib/plugins/recommend/style.css')}}">
<!-- Home -->
<link href="{{asset('style1.css')}}" rel="stylesheet">
<script language="javascript">
</script>
<div class="home">
	<div class="home_background parallax-window" data-parallax="scroll" style="background-image:url({{asset('images/menu.jpg')}})">
	</div>
		<!-- <div class="home_content">
			<div class="home_title">thông tin quán ăn</div>
		</div> -->
	</div>
	<!-- Search -->
	<div class="caviar-food-menu section-padding-150 clearfix">
		<div class="container">
			<div class="row">
				<div class="col-2">
					<div class="food-menu-title">
						<h2>Menu</h2>
					</div>
				</div>

				<div class="col-10">
					<div class="caviar-projects-menu">
						<div class="text-center portfolio-menu">
							<button class="active" data-filter="*">Tất cả</button>
							<button data-filter=".breakfast">Ăn sáng</button>
							<button data-filter=".lunch">Ăn trưa</button>
							<button data-filter=".dinner">Ăn tối</button>
						</div>
					</div>

					<div class="caviar-menu-slides owl-carousel clearfix">

						<div class="caviar-portfolio clearfix">
							<!-- Single Gallery Item -->
							@foreach($monans as $monan)
							<div class="single_menu_item breakfast wow fadeInUp">
								<div class="d-sm-flex align-items-center">
									<div class="dish-thumb">
										<img src="{{$monan->image}}" alt="" style="border-radius: 164px;">
									</div>
									<div class="dish-description">
										<h3>{{$monan->name}}</h3>
										<p>{{$monan->mota}}.</p>
									</div>
									<div class="dish-value">
										<h3>50.000 VND</h3>
									</div>
								</div>
							</div>
							@endforeach
							<!-- Single Gallery Item -->
							@foreach($monans1 as $monan)
							<div class="single_menu_item dinner wow fadeInUp">
								<div class="d-sm-flex align-items-center">
									<div class="dish-thumb">
										<img src="{{$monan->image}}" alt="" style="border-radius: 164px;">
									</div>
									<div class="dish-description">
										<h3>{{$monan->name}}</h3>
										<p>{{$monan->mota}}.</p>
									</div>
									<div class="dish-value">
										<h3>60.000 VND</h3>
									</div>
								</div>
							</div>
							@endforeach
							<!-- Single Gallery Item -->
							@foreach($monans2 as $monan)
							<div class="single_menu_item lunch wow fadeInUp">
								<div class="d-sm-flex align-items-center">
									<div class="dish-thumb">
										<img src="{{$monan->image}}" alt="" style="border-radius: 164px;">
									</div>
									<div class="dish-description">
										<h3>{{$monan->name}}</h3>
										<p>{{$monan->mota}}.</p>
									</div>
									<div class="dish-value">
										<h3>40.000 VND</h3>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	@endsection