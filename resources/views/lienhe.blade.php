@extends('layout.user.master')

@section('content')
	<!-- ***** Breadcumb Area Start ***** -->
    <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/hero-4.jpg)">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content">
                        <h2>Liên hệ</h2>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+1234567890">+84 1642 8989 57</a> 
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 484 Lê Văn Việt, phường Tăng Nhơn Phú A, Quận 9</p>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:longtrinhdu@gmail.com">longtrinhdu@gmail.com</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

	<div class="contact_form_section">
		<div class="container">
		<!-- <button  type="submit" class="form_submit_button button trans_200"><a href="/home">Quay lại</a></button> -->
		<p></p>
			<div class="row">
			
				<div class="col">
					<!-- Contact Form -->
					<div class="contact_form_container" id="contact_form_container">
						<div id="title_contact" class="contact_title text-center">Chia sẻ quán ăn</div>
						<!-- <div id="form-messages" class="contact_address_name"></div> -->
						<div id="contact_div">
						
							<form  method="post" id="contact_form" class="contact_form text-center" action="quanan/add">
								@csrf
								<input type="text" id="name"  name="tenquanan" class="contact_form_name input_field" placeholder="Nhập vào tên quán ăn" data-error="Name is required." style="font-size: 20px;" required>
								<input type="text" id="name"  name="worktime" class="contact_form_name input_field" placeholder="Nhập vào thời gian hoặt động" data-error="Name is required." style="font-size: 20px;" >
								<input type="text" id="name"  name="price" class="contact_form_name input_field" placeholder="Nhập vào giá" data-error="Name is required." style="font-size: 20px;" >
								<textarea id="contact_form_message" class="contact_form_message" name="message" rows="4" placeholder="Mô tả địa điểm ..." data-error="Please, write us a message."></textarea>
								
								<div class="extras">
									<div class="contact_theoai" >
										#Tag
									</div>
									<ul class="search_extras clearfix">
										@foreach($tags as $tag)
										<li class="search_extras_item">
											<div class="clearfix">
												<input type="checkbox" id="search_extras_1" class="checkbox" name="ChkID[]" value="{{$tag->no}}">
												<label for="search_extras_1">{{$tag->name}}</label>
											</div>	
										</li>
										@endforeach
									</ul>
								</div>
								<button type="submit" id="form_submit_button" class="form_submit_button button trans_200">Tiếp theo <span></span><span></span><span></span></button>
							</form>	
						</div>
						<script src="/javascripts/application.js" type="text/javascript" charset="utf-8" async defer>
							
						</script>	
						<div id="address_div">
							<form style="display: none;" method="post" id="address_form" class="contact_form text-center" action="quanan/add-address">
							@csrf
							<div id="form-address" class="select_contact d-flex justify-content-lg-between">
								<div class="search_item">
									@csrf
									<div>tỉnh</div>
									<select name="province" id="province_1" class="dropdown_item_select search_input" ">
										<option disabled selected value>Chọn địa điểm</option>
										@foreach($provinces as $province)
										<option value="{{$province->id}}">{{$province->_name}}</option>
										@endforeach
									</select>
								</div>
								<div class="search_item">
									<div>quận/huyện</div>
									<select name="district" id="district_1" class="dropdown_item_select search_input">
										
									</select>
								</div>
								<div class="search_item">
									<div>xã/phường</div>
									<select name="ward" id="ward_1" class="dropdown_item_select search_input">
										
									</select>
								</div>
								<div class="search_item">
									<div>đường/phố</div>
									<select name="street" id="street_1" class="dropdown_item_select search_input">
										
									</select>
								</div>
							 </div>
							<button type="submit" id="form_submit_button" class="form_submit_button button trans_200">Tiếp theo<span></span><span></span><span></span></button>

						</form>
					</div>
					
					</div>
					<div style="display:none;" id="map_form">
						<div  class="caviar-contact-area d-md-flex align-items-center">
				        <div class="contact-form-area d-flex justify-content-end">
				            <div class="contact-form">
				                <div id="title_contact" class="contact-form-title">
				                    <p>Thông tin địa chỉ trên bản đồ</p>
				                </div>
				                <!-- <div id="form-messages" class="contact_title text-center">a</div> -->
				                    <form method="post" id="gmap_form" class="contact_form text-center" action="quanan/add-map">
				                    @csrf	
				                    <div class="row">
				                        <div class="col-12">
				                            <input type="text" class="form-control" name="lat" id="lat" placeholder="Lat">
				                        </div>
				                        <div class="col-12">
				                            <input type="text" class="form-control"name="lng" id="lng"  placeholder="Lng">
				                        </div>
				                        <!-- <div class="col-12">
				                            <input type="text" class="form-control"name="name" >
				                        </div> -->
				                        <div class="col-12">
				                            <button type="submit" class="btn caviar-btn"><span></span> Tiếp theo</button>
				                        </div>
				                    </div>
				                </form>
				            </div>
				        </div>
				        <div class="caviar-map-area wow fadeInRightBig" data-wow-delay="0.5s">
				            <div id="googleMap"></div>
				        </div>
				    </div>
					</div>
					<div id="image_div">
						<form style="display: none;" id="image_form" action="/file" enctype="multipart/form-data" method="POST">
						        {{ csrf_field() }}
						        <input class="btn caviar-btn" type="file" name="filesTest" required="true">
						        <br/>
						        <button  type="submit" class="form_submit_button button trans_200">Gửi</button>
						  </form>
					</div>
				</div>
				</div>
			</div>
		</div>
<!-- 	<div id="form-messages"></div> -->
	<!-- Google Maps js -->
    <!-- Google Maps js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzbS2nerrvbdvNlsyQO9cDWFNthdoswRU"></script>    
    <script >
        var map;
        var latlng = new google.maps.LatLng(10.851249,106.772809);
        var stylez = [{
            featureType: "all",
            elementType: "all",
            stylers: [{
                saturation: -100
                    }]
                }];
        var mapOptions = {
            zoom: 15,
            center: latlng,
            disableDefaultUI: true,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'gMap']
            }
        };
         var infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

           var marker = new google.maps.Marker({
                    map: map,
                    draggable:true,
                    icon: 'img/core-img/point.png',
                    position: map.getCenter()
                }); 

        
          google.maps.event.addListener(marker,'click',function(){
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();
            $('#lat').val(lat);
            $('#lng').val(lng);
          });
            </script>
@endsection