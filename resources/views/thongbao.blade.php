@extends('layout.user.master')
@section('content')
	<!-- ***** Breadcumb Area Start ***** -->
    <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/hero-4.jpg)">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content">
                        <h2>Liên hệ</h2>
                        <p><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:+1234567890">+84 1642 8989 57</a> 
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 484 Lê Văn Việt, phường Tăng Nhơn Phú A, Quận 9</p>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:longtrinhdu@gmail.com">longtrinhdu@gmail.com</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->
	<!-- Home -->

	<div class="home_timkiem">
		<div class="home_background parallax-window" data-parallax="scroll" style="background-image:url(../images/blog_background.jpg)"></div>
		<div class="home_content">
			<div class="home_title"></div>
		</div>
	</div>
	<!-- Contact -->

	<div class="contact_form_section">
		<div class="container">
			<div class="row">
				<div class="col">

					<!-- Contact Form -->
						<div class="contact_form_container">
						<div id="title_contact" class="contact_title text-center">Hoàn thành chia sẻ
						</div>
						<div id="title_contact" class="contact_title text-center">đang chờ duyệt bởi người quản trị
						</div>
					</div>
				</div>
					<!-- Contact Form -->
				</div>
			</div>
		</div>
	</div>
	<div id="form-messages"></div>
@endsection