<!DOCTYPE html>
<html lang="en">
<head>
<title>Demo</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Travelix Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{asset('style.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('lib/styles/main_styles.css')}}">
<!-- <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css -->

<!-- <link rel="stylesheet" type="text/css" href="{{asset('lib/styles/bootstrap4/bootstrap.min.css')}}"> -->
<!-- <link href="{{asset('lib/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
 --><!-- <link rel="stylesheet" type="text/css" href="{{asset('lib/plugins/OwlCarousel2-2.2.1/owl.carousel1.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('lib/plugins/OwlCarousel2-2.2.1/owl.theme.defsault.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('lib/plugins/OwlCarousel2-2.2.1/animate.css')}}"> -->
<!-- <link rel="stylesheet" type="text/css" href="{{asset('lib/styles/responsive.css')}}">-->
<!-- <link href="{{asset('css/responsive/responsive.css')}}" rel="stylesheet"> -->

</head>
<body>
<!-- Preloader -->
    <div id="preloader">
        <div class="caviar-load"></div>
        <div class="preload-icons">
            <img class="preload-1" src="{{asset('img/core-img/preload-1.png')}}" alt="">
            <img class="preload-2" src="{{asset('img/core-img/preload-2.png')}}" alt="">
            <img class="preload-3" src="{{asset('img/core-img/preload-3.png')}}" alt="">
        </div>
    </div>

    <!-- ***** Search Form Area ***** -->
    <div class="caviar-search-form d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-close-btn" id="closeBtn">
                        <i class="pe-7s-close-circle" aria-hidden="true"></i>
                    </div>
                    <form action="{{url('searchByName')}}" method="post">
                        @csrf
                        <input type="search" name="caviarSearch" id="searchByName" placeholder="Tìm kiếm tên quán ăn ...">
                        <input type="submit" class="d-none" value="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>  
  <!-- Header -->
  <header class="header_area" id="header">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100">
                    <nav class="h-100 navbar navbar-expand-lg align-items-center">
                        <a class="navbar-brand" href="/home">Ẩm Thực Chay</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#caviarNav" aria-controls="caviarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="fa fa-bars"></span></button>
                        <div class="collapse navbar-collapse" id="caviarNav">
                            <ul class="navbar-nav ml-auto" id="caviarMenu">   
                             @if(Session::has('dangnhap'))
                          
                            <!-- <li class="nav-item active">
                                    <a class="nav-link" href="/home">HOME</a>
                                </li> -->
                            
                            <li class="nav-item"><a class="nav-link" href="{{url('lienhe')}}">Chia sẻ thêm quán ăn</a></li>
                            
                            <li class="nav-item dropdown" >
                                    <a id="checkBox" class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Session('dangnhap')->name}}</a>
                                    <div id="showcart" class="dropdown-menu" aria-labelledby="navbarDropdown">

                                        <a class="dropdown-item" href="/profile">THÔNG TIN TÀI KHOẢN</a>
                                        
                                        @if(Session::has('dangnhap') && Session('dangnhap')->state == 1)
                                        <a class="dropdown-item" href="/user">QUẢN LÝ USER</a>
                                        <a class="dropdown-item" href="/chiase">QUẢN LÝ CHIA SẺ </a>
                                        <a class="dropdown-item" href="/quanan">QUẢN LÝ QUÁN ĂN</a>
                                        <a class="dropdown-item" href="/monan">QUẢN LÝ MÓN ĂN</a>
                                        @endif
                                        <a class="dropdown-item" href="/dangxuat">ĐĂNG XUẤT</a>

                                    </div>
                                </li>
                            
                        @else
                        <!-- <li class="nav-item active">
                        <a class="nav-link" href="/home"><i class="fa fa-home"></i>HOME <span class="sr-only">(current)</span></a></li> -->
                        <li class="nav-item"><a class="nav-link"href="{{url('dangnhap')}}">ĐĂNG NHẬP</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{url('dangki')}}">ĐĂNG KÍ</a></li> 
                        @endif 
                            </ul>
                            <!-- Search Btn -->
                            <div class="caviar-search-btn">
                                <a id="search-btn" href="#"><i class="fa fa-search" aria-hidden="true"> &nbsp Tìm kiếm</i></a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>


<script>
  /*  const showcart = document.querySelector('#showcart');
    const checkBox = document.querySelector('#checkBox');
    checkBox.onclick = () =>{
      if(showcart.style.display=='block')
        showcart.style.display='none';
      else  
        showcart.style.display='block';
    }
    showcart.onmouseover = () =>{
      showcart.style.display='block';
    }
    showcart.onmouseleave = () =>{
      showcart.style.display='none';
    }*/
  </script>