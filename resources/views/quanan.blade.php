@extends('layout.user.master')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('lib/styles/single_listing_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('lib/styles/single_listing_responsive.css')}}">
<!-- Recommend -->
<link rel="stylesheet" href="{{asset('lib/plugins/recommend/icomoon.css')}}">
<!-- Owl Carousel -->
<link rel="stylesheet" href="{{asset('lib/plugins/recommend/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('lib/plugins/recommend/owl.theme.default.min.css')}}">
<!-- Theme style  -->
<link rel="stylesheet" href="{{asset('lib/plugins/recommend/style.css')}}">
<!-- Home -->
<link href="{{asset('style1.css')}}" rel="stylesheet">
 <script language="javascript">
		function CheckFrm(){

			var form = document.frmrating;
			var str = form.comment.value;
			var count = str.length;

			if (count < 3){
				alert('Mời bạn đánh giá sản phẩm.(Tối thiểu 3 ký tự)');
			}
			else if (form.TxtUserID.value == ''){
				alert('Vui lòng đăng nhập trước khi đánh giá.');
			}
			else
				form.submit();
			
		}		
	</script>
	<div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" style="background-image:url({{asset('images/about_background.jpg')}})">
		</div>
		<!-- <div class="home_content">
			<div class="home_title">thông tin quán ăn</div>
		</div> -->
	</div>
		<!-- Search -->
		<!-- Single Listing -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="single_listing">
					<!-- Hotel Info -->
					<div class="hotel_info">
						<!-- Listing Image -->
						<div class="row">
							<div class="col-lg-6">
								@if($quanan->image_local)
								<div class="hotel_image">
									<img src="{{asset('images/quanan/'.$quanan->image_local)}}" alt="">
								</div>
								@else 
								<div class="hotel_image">
									<img src="{{asset($quanan->image)}}" alt="">
								</div>
								@endif
							</div>
							<div class="col-lg-6">
								<div class="row">
									<div class="col-lg-10">
										<div class="hotel_location">
											<b> {{$address->provinces->_name}} - {{$address->districts->_name}}</b>
										</div>
										<div class="mota_text"><u>@foreach($motas as $mota)  {{$mota->name}},  @endforeach</u></div>
										<div class="hotel_title_container d-flex flex-lg-row flex-column">
											<div class="hotel_title_content">
												<h1 class="hotel_title">{{$quanan->name}}</h1>
												<p>{{$quanan->diachi}}</p>
											</div>
										</div>
									</div>
									@if($address->rates != null)
									<div class="col-lg-2">
										<div class="hotel_review_rating">{{number_format($address->rates->average*2,1)}}</div>
										<div style="padding-left: 40px;padding-top: 10px;width: max-content;">{{$address->rates->total_rate}} đánh giá</div>
									</div>
									@endif
								</div>
								<div class="row">
									<div class="col-lg-12 time_text"><span></span>Hoạt động : {{$quanan->worktime}}</div>
									<div class="col-lg-12 price_text">Giá : {{$quanan->price}} (VND)</div>
								</div>
								<div class="row">
									<div class="hotel_title_button">
										<div class="hotel_map_link_container">
											<div class="hotel_map_link"><a href="#location_on_map">Xem trên bản đồ</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="caviar-dish-menu" id="menu">
				        <div class="container">
				            <div class="row">
				                <div class="col-12 menu-heading">
				                    <div class="section-heading text-center">
				                        <h2>Đặc biệt</h2>
				                    </div>
				                    <!-- btn -->
				                    <a href="/menu" class="btn caviar-btn"><span></span> <p class="menu_text">Thực đơn</p></a>
				                </div>
				            </div>
				            <div class="row">
				                <div class="col-12 col-sm-6 col-md-4">
				                    <div class="caviar-single-dish wow fadeInUp" data-wow-delay="0.5s">
				                        <img style="border-radius: 50%;" src="https://media.cooky.vn/recipe/g5/43391/s320x320/cooky-recipe-cover-r43391.jpg" alt="">
				                        <div class="dish-info">
				                            <h6 class="dish-name">Cà rốt xào sốt thơm</h6>
				                            <p class="dish-price">45.000đ </p>
				                        </div>
				                    </div>
				                </div>
				                <div class="col-12 col-sm-6 col-md-4">
				                    <div class="caviar-single-dish wow fadeInUp" data-wow-delay="1s">
				                       <img style="border-radius: 50%;" src="https://media.cooky.vn/recipe/g1/4437/s320x320/recipe4437-635991860267278669.jpg" alt="">
				                        <div class="dish-info">
				                            <h6 class="dish-name">Chả giò bắp</h6>
				                            <p class="dish-price">45.000đ </p>
				                        </div>
				                    </div>
				                </div>
				                <div class="col-12 col-sm-6 col-md-4">
				                    <div class="caviar-single-dish wow fadeInUp" data-wow-delay="1.5s">
				                       <img style="border-radius: 50%;" src="https://media.cooky.vn/recipe/g1/1818/s320x320/recipe1818-635687501691244634.jpg" alt="">
				                        <div class="dish-info">
				                            <h6 class="dish-name">Lagu chay</h6>
				                            <p class="dish-price">45.00đ </p>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				     <div class="rating-content">
				  		<div class="row">
								<div class="col-md-6 col-md-offset-3 colorlib-heading animate-box">
									<h3 style="padding-bottom: 14px;">Đánh giá</h3>
								</div>
						</div>
					    <div class="row">
					    	@if($ratings->isEmpty()!=true)
					    	<div class="col-sm-6">
							<hr>
								<div class="review-block">
								@foreach($ratings as $rating)
								  <div class="row">
								    <div class="col-sm-3">
						            <img style="height: 85px;" src="https://image.flaticon.com/icons/svg/149/149071.svg" class="img-rounded">
								      <div class="review-block-name"><a href="#">{{$rating->name}}</a></div>
								      <div class="review-block-date">{{date('d-m-Y H:i:s', strtotime($rating->created_at))}}</div>
								    </div>
								    <div class="col-sm-9">
								      <div class="review-block-rate">
								       @if($rating->rate > 0)
					                        @for ($i = 0; $i < $rating->rate; $i++)
											    <button type="button" class="btn btn-warning btn-xs" aria-label="Left Align">★</button>
											@endfor
				                        @endif
				                    	@if($rating->rate < 5)
				                    		@for ($i = $rating->rate; $i < 5; $i++)
										    <button type="button" class="btn btn-grey btn-xs" aria-label="Left Align">★</button>
											@endfor
				                        @endif     
								      </div>
								      <div class="review-block-title">{{$rating->title}}</div>
								      <div class="review-block-description">{{$rating->comment}}</div>
								    </div>
								  </div>
								  <hr>
								   @endforeach
								</div>
							</div>
					        <div class="col-sm-6">
					        	<div style="margin-top: 34px;" class="row">
					        		<div class="col-sm-5">
						                <div class="rating-block">
						                    <h4>Đánh giá trung bình</h4>
						                    <h2 style="color: red;" class="bold padding-bottom-7">{{$address->rates->average}} <small>/ 5</small></h2>
						                    @if(round($address->rates->average) > 0)
					                      	  @for ($i = 0; $i < round($address->rates->average); $i++)
						                    <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
						                        ★
						                    </button>
						                   	 @endfor
						                   	 @endif
						                 	@if(round($address->rates->average) < 5)
				                    			@for ($i = round($address->rates->average); $i < 5; $i++)
						                    <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
						                       ★
						                    </button>
							                    @endfor
					                        @endif 
					                         <div style="padding: 10px;">({{$address->rates->total_rate}} đánh giá)</div>
						                </div>
						               
					            	</div>
						           	<div class="col-sm-7">
						                <h4>Thống kê</h4>
						                <div class="pull-left">
						                    <div class="pull-left" style="width:35px; line-height:1;">
						                        <div style="height:9px; margin:5px 0;">5 ★</div>
						                    </div>
						                    <div class="pull-left" style="width:180px;">
						                        <div class="progress" style="height:9px; margin:8px 0;">
						                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 100%">
						                            </div>
						                        </div>
						                    </div>
						                    <div class="pull-right" style="margin-left:-1px;">{{$address->rates->star5}} đánh giá</div>
						                </div>
						                <div class="pull-left">
						                    <div class="pull-left" style="width:35px; line-height:1;">
						                        <div style="height:9px; margin:5px 0;">4 ★</div>
						                    </div>
						                    <div class="pull-left" style="width:180px;">
						                        <div class="progress" style="height:9px; margin:8px 0;">
						                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%">
						                            </div>
						                        </div>
						                    </div>
						                    <div class="pull-right" style="margin-left:-1px;">{{$address->rates->star4}} đánh giá</div>
						                </div>
						                <div class="pull-left">
						                    <div class="pull-left" style="width:35px; line-height:1;">
						                        <div style="height:9px; margin:5px 0;">3 ★</div>
						                    </div>
						                    <div class="pull-left" style="width:180px;">
						                        <div class="progress" style="height:9px; margin:8px 0;">
						                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%">
						                            </div>
						                        </div>
						                    </div>
						                    <div class="pull-right" style="margin-left:-1px;">{{$address->rates->star3}} đánh giá</div>
						                </div>
						                <div class="pull-left">
						                    <div class="pull-left" style="width:35px; line-height:1;">
						                        <div style="height:9px; margin:5px 0;">2 ★</div>
						                    </div>
						                    <div class="pull-left" style="width:180px;">
						                        <div class="progress" style="height:9px; margin:8px 0;">
						                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%">
						                            </div>
						                        </div>
						                    </div>
						                    <div class="pull-right" style="margin-left:-1px;">{{$address->rates->star2}} đánh giá</div>
						                </div>
						                <div class="pull-left">
						                    <div class="pull-left" style="width:35px; line-height:1;">
						                        <div style="height:9px; margin:5px 0;">1 ★</div>
						                    </div>
						                    <div class="pull-left" style="width:180px;">
						                        <div class="progress" style="height:9px; margin:8px 0;">
						                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%">
						                            </div>
						                        </div>
						                    </div>
						                    <div class="pull-right" style="margin-left:-1px;">{{$address->rates->star1}} đánh giá</div>
						                </div>
						            </div>
					        	</div>
					        	<div class="row" style="margin-top:40px;">
					        		<div class="col-sm-12">
					        			<div style="padding: 15px;"class="text-center">
							                <a class="btn btn-success btn-green" href="#" id="open-review-box">Viết nhận xét của bạn</a>
							            </div>
							            <div class="row" id="post-review-box" style="display:none;">
							                <div class="col-sm-12">
							                    <form accept-charset="UTF-8" action="{{action('U_homepageController@danhgia',$quanan->id)}}" method="post" name="frmrating"> @csrf 
							                    	<input type="hidden" name="TxtUserID" value="@if(Session::has('dangnhap')){{{Session('dangnhap')->id}}} 
							                    	@endif" />
							                        <textarea class="form-control animated" cols="50" id="new-review" name="title" placeholder="Nhập tiêu đề để nhận xét (Không bắt buộc)" rows="3"></textarea>
							                         <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Viết nhận xét tại đây..." rows="5"></textarea>
							                         <div class="text-right">
							                         	<div class="row">
							                         		<div style="margin-top: 10px;;"class="col-sm-6">
							                         			<p style="font-size: 15.2px "> Đánh giá của bạn về sản phẩm này :</p>
							                         		</div>
							                         		<div class="col-sm-6">
							                         			<div class="rate">
							                         				<input type="radio" id="star5" name="rate" value="5" />
							                         				<label for="star5" title="text">5 stars</label>
							                         				<input type="radio" id="star4" name="rate" value="4" />
							                         				<label for="star4" title="text">4 stars</label>
							                         				<input type="radio" id="star3" name="rate" value="3" />
							                         				<label for="star3" title="text">3 stars</label>
							                         				<input type="radio" id="star2" name="rate" value="2" />
							                         				<label for="star2" title="text">2 stars</label>
							                         				<input type="radio" id="star1" name="rate" value="1" />
							                         				<label for="star1" title="text">1 star</label>
							                         			</div>
							                         		</div>
							                         		</div>
							                         	<a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
							                         		<span class="glyphicon glyphicon-remove"></span>Hủy</a>
							                         		<input type="button" class="btn btn-success btn-lg" name="btn" value="Gửi" onclick="CheckFrm()">
							                         	</div>
							                    </form>
							                </div>
							            </div>
							        </div> 
								</div>
					        </div>
					        @else
							<div class="col-sm-6 hotel_location">Hãy là người đánh giá đầu tiên.
							<div class="row" style="margin-top:40px;">
							    	<div class="col-sm-12">
							            <div class="text-left">
							                <a class="btn btn-success btn-green" href="#" id="open-review-box">Để lại đánh giá</a>
							            </div>
							            <div class="row" id="post-review-box" style="display:none;">
							                <div class="col-sm-12">
							                    <form accept-charset="UTF-8" action="{{action('U_homepageController@danhgia',$quanan->id)}}" method="post" name="frmrating"> @csrf 
							                    	
							                    	<input type="hidden" name="TxtUserID" value="@if(Session::has('dangnhap')){{{Session('dangnhap')->id}}} 
							                    	@endif" />
							                        <textarea class="form-control animated" cols="50" id="new-review" name="title" placeholder="Nhập tiêu đề để nhận xét (Không bắt buộc)" rows="3"></textarea>
							                         <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Viết nhận xét tại đây..." rows="5"></textarea>
							                        <div class="text-right">
							                              <div class="rate">
																<input type="radio" id="star5" name="rate" value="5" />
																<label for="star5" title="text">5 stars</label>
																<input type="radio" id="star4" name="rate" value="4" />
																<label for="star4" title="text">4 stars</label>
																<input type="radio" id="star3" name="rate" value="3" />
																<label for="star3" title="text">3 stars</label>
																<input type="radio" id="star2" name="rate" value="2" />
																<label for="star2" title="text">2 stars</label>
																<input type="radio" id="star1" name="rate" value="1" />
																<label for="star1" title="text">1 star</label>
															</div>
							                            <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
							                            <span class="glyphicon glyphicon-remove"></span>Hủy</a>
							                            <input type="button" class="btn btn-success btn-lg" name="btn" value="Gửi" onclick="CheckFrm()">
							                        </div>
							                    </form>
							                </div>
							            </div>
							        </div> 
								</div>
							</div>
							@endif
					    </div>
					</div>
				  	<div id="colorlib-hotel">
						<div class="row">
							<div class="col-md-6 col-md-offset-3 location_on_map_title">
								<h4 style="padding-bottom: 14px;">Các địa điểm liên quan <span style="color: crimson;">({{count($ds_goi_y1)}})</span></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 animate-box">
								<div class="owl-carousel">
									@foreach($ds_goi_y1 as $item)
									<div class="item">
										<div class="hotel-entry">
											<a href="{{action('U_homepageController@ChitietQuanan', $item->id)}}" class="hotel-img" style="background-image: url({{$item->image}});">
												<p class="price"><span>{{number_format($item->average*2,1)}}</span></p>
											</a>
											<div class="desc">
												<h4><a href="#">{{$item->name}}</a></h4>
												<span class="place">{{$item->diachi}}</span>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</div>
						@if($_isNotLogin == 0)
						<div class="row">
							<div class="col-md-6 col-md-offset-3 location_on_map_title">
								<h4 style="padding-bottom: 14px;">Các địa điểm khác<span style="color: crimson;"></h4>
							</div>
						</div>
						@else
						<div class="row">
							<div class="col-md-6 col-md-offset-3 location_on_map_title">
								<h4 style="padding-bottom: 14px;">Gợi ý cho bạn<span style="color: crimson;">({{count($ds_goi_y2)}})</span></h4>
							</div>
						</div>
						@endif
						
						<div class="row">
							<div class="col-md-12 animate-box">
								<div class="owl-carousel">
									@foreach($ds_goi_y2 as $item)
									<div class="item">
										<div class="hotel-entry">
											<a href="{{action('U_homepageController@ChitietQuanan', $item->id)}}" class="hotel-img" style="background-image: url({{$item->image}});">
												<p class="price"><span>{{number_format($item->average*2,1)}}</span></p>
											</a>
											<div class="desc">
												<h4><a href="#">{{$item->name}}</a></h4>
												<span class="place">{{$item->diachi}}</span>
											</div>
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>

					<!-- Reviews -->
					<!-- Location on Map -->
					<div style="padding-top: 90px;" id="location_on_map" ></div>
					<div  id="colorlib-map">
						<div class="location_on_map_title">Vị trí trên bản đồ</div>
						<!-- Google Map -->
						<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzbS2nerrvbdvNlsyQO9cDWFNthdoswRU"></script>
							<div class="travelix_map">
								<div id="google_map" class="google_map">
									<div class="map_container">
										<div id="map"></div>
										<script>
										var myLatlng = new google.maps.LatLng({{$quanan->address[0]->lat}},
			 								{{$quanan->address[0]->lng}});
										var mapOptions = {
										center: myLatlng,
										zoom: 13,
										mapTypeId: google.maps.MapTypeId.ROADMAP
										};
										var map = new google.maps.Map(document.getElementById("map"),
										mapOptions);	

										var myLatlng1 = new google.maps.LatLng({{$quanan->address[0]->lat}},
			 								{{$quanan->address[0]->lng}});

										var marker = new google.maps.Marker({
										position: myLatlng1,
										map: map,
										icon: '../img/core-img/MapCorrect-green.png',
										title: 'Quán {{$quanan->name}}'
										});
										var infoWindow = new google.maps.InfoWindow();

										google.maps.event.addListener(marker, 'click', function (e) {
												infoWindow.open(map, marker);
										});

										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
	@endsection