
<html>
  <head>
    <meta charset="utf-8">
    <title>Map</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <link rel="stylesheet" type="text/css" href="{{asset('css/mapcanvas.css')}}">
    <link href="https://code.ionicframework.com/nightly/css/ionic.css" rel="stylesheet">
    <script src="https://code.ionicframework.com/nightly/js/ionic.bundle.js"></script>
    <!-- google maps javascript -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzbS2nerrvbdvNlsyQO9cDWFNthdoswRU"></script>
  </head>
  <body ng-controller="MapCtrl">
    <div class="bar-dark bar bar-header">
      <h1 class="title">Quán Chay MAP</h1>
      <a style="font-size: 15px;position: inherit;cursor: pointer;" href="/home">Quay về trang chủ</a>
    </div>

      <div id="map" data-tap-disabled="true"></div>
      <script>
      	var myLatlng = new google.maps.LatLng({{$province->lat}},{{$province->lng}});
		var mapOptions = {
        center: myLatlng,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
     	var map = new google.maps.Map(document.getElementById("map"),
          mapOptions);	

     	@foreach($quanans as $quanan)
      @if($quanan->address[0]->province == $khuvuc)
      var contentString = "<div><a href='/quananid/{{$quanan->id}}'>Xem quán ăn!</a></div>";
     	var myLatlng1 = new google.maps.LatLng({{$quanan->address[0]->lat}},
     		{{$quanan->address[0]->lng}});

     	var marker = new google.maps.Marker({
          position: myLatlng1,
          map: map,
           icon: 'img/core-img/red-1.png',
          title: 'Quán {{$quanan->name}}'
        });
      var infoWindow = new google.maps.InfoWindow();
         (function (marker, contentString) {
                     google.maps.event.addListener(marker, 'click', function (e) {
                         infoWindow.setContent(contentString);
                         infoWindow.open(map, marker);
                         console.log("s00");
                     });
                 })(marker, contentString);
         @endif
        @endforeach
    
      </script>
    <!-- <div class="bar-dark bar bar-footer">
     <a ng-click="centerOnMe()" class="button button-icon icon ion-navigate">Vị trí của bạn</a>
    </div> -->
  </body>
  <!-- <script src="{{asset('js/mapscanvas.js')}}"></script> -->
  