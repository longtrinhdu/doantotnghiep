@extends('layout.admin.master')
@section('content')
  <body>
    <div class="container">
    <br />
    <div class="row">
      
    </div>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped ">
    <thead>
      <tr>
         <th>STT</th>
        <th>TÊN QUÁN ĂN</th>
        <th>TỈNH</th>
        <th>QUẬN/HUYỆN</th>
        <th>ĐƯỜNG/PHỐ</th>
        <th width="30%">XÃ/PHƯỜNG/THỊ TRẤN</th>
        
        <th colspan="2">ACTION</th>
      </tr>
    </thead>
    <tbody>
      <?php $i=1; ?>
      @foreach($diachi as $address)
      @if($address->quanans->state == 1)
      <tr>
        <td >{{$i++}}</td>
        <td width="20%">{{$address->quanans['name']}}</td>
        <td>{{$address->provinces['name'] }}</td>
        <td>{{$address->districts['name']}}</td>
        <td>{{$address->streets['name']}}</td>
        <td>{{$address->wards['name']}}</td>        
        <td><a href="{{action('AddressController@edit', $address->id)}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('AddressController@delete', $address->id)}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
        
      </tr>
      @endif
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
@endsection