@extends('layout/admin/master')
@section('content')
  <body style="background-image:url({{asset('images/adv/home_slider2.jpg')}})">
    <div class="container">
    <br />
     <div style="
    text-align: center;
    color: antiquewhite;
    font-size: 40px;
">
        Duyệt Chia Sẻ
      </div>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped text-info">
    <thead>
      <tr>
        
        <th>EMAIL</th>
        <th>TÊN QUÁN ĂN</th>
        <th>TỈNH</th>
        <th>QUẬN/HUYỆN</th>
        <th>ĐƯỜNG/PHỐ</th>
        <th>XÃ/PHƯỜNG/THỊ TRẤN</th>
        <th>Ảnh</th>
        <th colspan="2">ACTION</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($quanans as $quanan)
      <tr>
        <td>user</td>
        <td>{{$quanan->name}}</td>
        <td>{{$quanan->address[0]->provinces['name']}}</td>
        <td>{{$quanan->address[0]->districts['name']}}</td>
        <td>{{$quanan->address[0]->wards['name']}}</td>
        <td>{{$quanan->address[0]->streets['name']}}</td>
        <td><img src="{{asset('images/quanan/'.$quanan->image)}}" alt="" border=3 height=100 width=200></img></td>
      

        <td><a href="{{action('PageController@duyet', $quanan->id)}}" class="btn btn-warning">YES</a></td>
        <td>
          <form action="{{action('PageController@delete1', $quanan->id)}}" method="post">
            @csrf
           
            <input name="_method" type="hidden" value="DELETE">
             <input name="id_address" type="hidden" value="{{$quanan->address[0]->id}}">
            <button class="btn btn-danger" type="submit">NO</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
@endsection