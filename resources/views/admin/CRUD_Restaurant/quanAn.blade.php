@extends('layout.admin.master')
@section('content')
  <body>
    <div>
      <!--  <a href="{{url('/quanan/create')}}" class="btn btn-danger">Them quan an</a>
    <br /> -->
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>STT</th>
        <th>Tên quán ăn</th>
        <th>Số like</th>
        <th>Nhân viên phục vụ</th>
        <th>Chất lượng đồ ăn</th>
        <th>Không gian thoải mái</th>
        <th>Giá tiền</th>
        <th>Địa điểm dễ tìm</th>
         <th>Ảnh</th>
        <th colspan="3"></th>
      </tr>
    </thead>
    <tbody>
      <?php $i = 1 ;?>
      @foreach($quanans as $quanan)
       
      <tr>
        <th width="4%"><input type="checkbox" id="ChkAllID" name="ChkAllID" onchange="" /></th>
        <td>{{$i++}}</td>
        <td>{{$quanan->name}}</td>
        <td>{{$quanan->soLike}}</td>
        <td>{{$quanan->vote1}}</td>
        <td>{{$quanan->vote2}}</td>
        <td>{{$quanan->vote3}}</td>
        <td>{{$quanan->vote4}}</td>
        <td>{{$quanan->vote5}}</td>  
         <td><img src="{{asset('images/quanan/'.$quanan->image)}}" alt="" border=3 height=100 width=100></img></td>
        <td><a href="{{action('quanAnController@edit', $quanan->id)}}" class="btn btn-success">Chỉnh sửa</a></td>                <td><a href="{{action('AddressController@index', $quanan->id)}}" class="btn btn-success">Xem địa chỉ</a></td>
        <td><a href="{{action('monAnController@index', $quanan->id)}}" class="btn btn-success">Xem món ăn</a></td>
     <!--    <td><a href="{{action('CommentController@index', $quanan->id)}}" class="btn btn-success">View Comment</a></td>
        <td><a href="{{action('CommentController@create', $quanan->id)}}" class="btn btn-warning">Add Comment</a></td> -->
        <td>
          <form action="{{action('quanAnController@delete',$quanan->id)}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <input name="id_address" type="hidden" value="{{$quanan->address[0]->id}}">
            <button class="btn btn-danger" type="submit">Xóa</button>
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
   <div style="float: right; margin-right: 15px " class="row">{{$quanans->links()}}</div>
  </div>
  </body>
@endsection