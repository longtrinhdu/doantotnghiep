@extends('layout.admin.master')
@section('content')
  <body >
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
     <div><h3>Danh sách món ăn của {{$monans[0]->quanans->name}} </h3></div>
    <table class="table table-striped">
    <thead>
      <tr>
        <th>STT</th>
        <th>Ảnh</th>
        <th>Tên món ăn</th>
        <th>Tính theo</th>
        <th>Giá</th>
        <th>Lượt thích</th>
        <th colspan="4">Action</th>
        <th><a href="{{url('create_monan')}}" class="btn btn-success">THêm món ăn</a></th>
      </tr>
    </thead>
    <tbody>
      <?php $i = 1 ;?>
      @foreach($monans as $monan)
      <tr>
        <td>{{$i++}}</td>
        <td><img src="{{asset('images/'.$monan->image)}}" alt="" border=3 height=100 width=100></img></td>
        <td>{{$monan->name}}</td>
        <td>{{$monan->moTa}}</td>
        <td>{{$monan->price}}</td>
        <td>{{$monan->soLike}}<td>
        <td><a href="{{action('monAnController@edit', $monan->_id)}}" class="btn btn-warning">Chỉnh sửa</a></td>
        <td><a href="{{action('Comment_maController@index', $monan->id)}}" class="btn btn-warning">Xem bình luận</a></td>
        <td><a href="{{action('Comment_maController@create', $monan->id)}}" class="btn btn-warning">Thêm bình luận</a></td>
        <td>
          <form action="{{action('monAnController@delete',$monan->id)}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Xóa</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @if(Session('xemmonan') != 1)
  <div style="float: right; margin-right: 15px " class="row">{{$monans->links()}}</div>
  @endif
  </div>
  </body>
@endsection