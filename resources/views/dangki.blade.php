<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Quan An</title>
  <!--
    Template 2105 Input
  http://www.tooplate.com/view/2105-input
  -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/materialize.min.css">
    <link rel="stylesheet" href="css/tooplate.css">
</head>

<body id="login"style="background-image:url(images/register_background.jpg)">

    <div class="container">
    
        <div class="row tm-register-row tm-mb-35">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 tm-login-l">
              @if(!Session::has('success_dangki'))
                <form action="create" method="post" class="tm-bg-black p-5 h-100">
                    <div class="input-field">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                    <input placeholder="Tên tài khoản" id="email" name="email" type="email" class="validate"required>
                    </div>
                    <div class="input-field">
                    <input placeholder="Mật khẩu" name="password" type="password" class="validate"required>
                    </div>
                    <div class="input-field">
                    <input placeholder="Tên hiển thị"  name="name" type="text" class="validate"required>
                    </div>
                    <div class="input-field">
                    <input placeholder="CMND" name="cmnd" type="number" step="1"  class="validate"required>
                    </div>
                    <div class="row">
                    <div class="col-md-4"> Giới tính  : </div>
                    <div class="form-group col-md-6">
                    <label class="col-md-4">
                        <input class="with-gap" name="gender" type="radio" value="Nam" checked>
                        <span>Nam </span>
                    </label>
                    <label>  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
                    <label class="col-md-4">
                        <input class="with-gap" name="gender" type="radio" value="Nu" >
                        <span>Nữ</span>
                    </label>
                       </div>
                  </div>
                  @if(Session::get('fail_dangki'))
                  <div style="margin-bottom: 21px; background: red;">{{ Session::get('fail_dangki') }}</div>
                  @endif
                    <div class="tm-flex-lr"> 
                      <button type="submit" class="btn btn-danger">Đăng kí tài khoản</button>
                    </div>
                </form>
                @endif
            </div>
             @if(Session::has('success_dangki'))
            <div style="margin-top: -161px;margin-left: -185px;">
                <header class="font-weight-light tm-bg-black p-5 h-100">
                    <h3 class="mt-0 text-white font-weight-light">Trang đăng ký</h3>
                     <p>{{ Session::get('success_dangki') }}</p>
                </header>
            </div>
             @else
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 tm-login-r">
                <header class="font-weight-light tm-bg-black p-5 h-100">
                    <h3 class="mt-0 text-white font-weight-light">Trang đăng ký</h3>
                    <p>Sau khi đăng nhập bạn có thể chia sẻ những quán ăn ngon cho mọi người.</p>
                    <p class="mb-0">Comment những quán ăn yêu thích,món ăn yêu thích,trãi nghiệm tìm kiếm quán ăn nhanh nhất mà bạn muốn!</p>
                </header>
            </div>
             @endif
        </div>
        <div class="row">
           @if(Session::has('success_dangki'))
            {{Session::forget('success_dangki')}}
            <div>
                <a href="/dangnhap" class="waves-effect btn-large btn-large-white px-4 black-text rounded-0">Đi đến trang đăng nhập</a>
            </div>
              @else
             <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ml-auto mr-0 text-center">
                 <a href="/home" class="waves-effect btn-large btn-large-white px-4 black-text rounded-0">Quay lại trang chủ</a>
            </div>
             @endif
        </div>
        <footer class="row tm-mt-big mb-3">
            <div class="col-xl-12 text-center">
                <p class="d-inline-block tm-bg-black white-text py-2 tm-px-5">
                    Long &copy; 2018 Quan An
                    
                </p>
            </div>
        </footer>
    </div>

    <script src="js/jquery-3.2.1.slim.min.js"></script>
    <script src="js/materialize.min.js"></script>

</body>

</html>
