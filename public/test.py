# Import các thư viện sử dụng
import sys
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'
import sqlalchemy
from rake_nltk import Rake
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer
# Kết nối đến database để lấy dữ liệu 
engine = sqlalchemy.create_engine('mysql+pymysql://root:@localhost:3306/doantotnghiep?charset=utf8', encoding="utf8")
df = pd.read_sql_table('tb_quanan',engine)
df = df[['id','mota','worktime','price']]
# Xử lý thông tin trong cột để tạo ra bộ profile của mỗi item
df['mota'] = df['mota'].map(lambda x: x.split(','))
df['worktime'] = df['worktime'].map(lambda x: x.split('-'))
df['price'] = df['price'].map(lambda x: x.split('-'))
for index, row in df.iterrows():
    row['mota'] = [x.lower().replace(' ','') for x in row['mota']]
    row['worktime'] = [x.lower().replace(':','h') for x in row['worktime']]
    df.at[df.index[index], 'mota'] = row['mota']
    df.at[df.index[index], 'worktime'] = row['worktime'] 
# Set index là cột ID
df.set_index('id', inplace = True)
# Sáp nhập lại các thuộc tích với nhau tạo ra bộ dữ liệu để tiến hành vectorization
df['bag_of_words'] = ''
columns = df.columns
for index, row in df.iterrows():
    words = ''
    for col in columns:
        words = words + ' '.join(row[col])+ ' '
    row['bag_of_words'] = words
#Các thông tin sáp nhập được lưu vào cột 'bag_of_words'

df.drop(columns = [col for col in df.columns if col!= 'bag_of_words'], inplace = True)

# Dùng CountVecorizer để vector hóa tạo ra ma trận đếm để áp dụng vào cosine_similarity 
count = CountVectorizer()
count_matrix = count.fit_transform(df['bag_of_words'])

# tạo ma trận cosine_similarity
cosine_sim = cosine_similarity(count_matrix, count_matrix)

# tạo Sê-ri theo index để chúng được liên kết với ID của các Item
indices = pd.Series(df.index)

# Hàm gợi ý
def recommendations(ID, cosine_sim = cosine_sim):
    recommended_food = []
    # Lấy ra chỉ số index ứng với ID truyền vào
    idx = indices[indices == ID].index[0]
    # Tạo ra bộ các item được sắp xếp giảm dần theo độ tương tự .
    score_series = pd.Series(cosine_sim[idx]).sort_values(ascending = False)
    # Lấy 10 item gần nhất
    top_10_indexes = list(score_series.iloc[1:11].index)
    # Lưu xuống database lại
    sql = "DELETE FROM tb_cb_item WHERE item = "+str(ID)
    with engine.begin() as conn:     # TRANSACTION
        conn.execute(sql)
    for i in top_10_indexes:
        sql = "INSERT INTO tb_cb_item VALUES ("+str(ID)+","+str(list(df.index)[i])+")"
        with engine.begin() as conn:     # TRANSACTION
            conn.execute(sql)
    for i in top_10_indexes:
        recommended_food.append(list(df.index)[i])
    return recommended_food
   
if __name__ == "__main__":
    itemID = int(sys.argv[1])
    recommendations(itemID)