# Import thư viện  
import sys
import pandas as pd 
import numpy as np
import sqlalchemy
from sklearn.metrics.pairwise import cosine_similarity
from scipy import sparse

engine = sqlalchemy.create_engine('mysql+pymysql://root:@localhost:3306/doantotnghiep?charset=utf8', encoding="utf8")
class CF(object):
    # Dữ liệu đầu vào của hàm khởi tạo class CF là ma trận Utility Y_data  
    # k là số lượng các điểm lân cận được sử dụng để dự đoán kết quả  
    # dist_func là hàm đó similarity giữa hai vectors  
    # Biến uuCF thể hiện việc đang sử dụng User-user CF (1) hay Item-item CF(0).  
    def __init__(self, Y_data, k, dist_func = cosine_similarity, uuCF = 1):
        self.uuCF = uuCF # user-user (1) or item-item (0) CF
        self.Y_data = Y_data if uuCF else Y_data[:, [1, 0, 2]]
        self.k = k # number of neighbor points
        self.dist_func = dist_func
        self.Ybar_data = None
        # number of users and items. Remember to add 1 since id starts from 0
        self.n_users = int(np.max(self.Y_data[:, 0])) + 1 
        self.n_items = int(np.max(self.Y_data[:, 1])) + 1
    def normalize_Y(self):
        users = self.Y_data[:, 0] # all users - first col of the Y_data
        self.Ybar_data = self.Y_data.copy()
        self.mu = np.zeros((self.n_users,))
        for n in range(self.n_users):
            # Lấy index theo id user
            ids = np.where(users == n)[0].astype(np.int32) 
            # Lấy id item theo id user  
            item_ids = self.Y_data[ids, 1] 
            # Lấy chỉ số rate 
            ratings = self.Y_data[ids, 2]
            # tính giá trị trung bình của ratings cho mỗi user  
            m = np.mean(ratings) 
            if np.isnan(m):
                m = 0 # to avoid empty array and nan value
            # normalize
            self.mu[n] = m
            self.Ybar_data[ids, 2] = ratings - self.mu[n]

        ################################################
         # hình thành ma trận đánh giá dưới dạng sparse matrix. 'Sparsity is important'
         # Ví dụ: nếu #user = 1M,  #item = 100k, 
         # thì hình dạng của rating matrix sẽ là (100k, 1M)
         # ta có thể không có đủ bộ nhớ để lưu trữ này
         # thay vào đó ta chỉ lưu nonzeros, và vị trí của chúng.
        self.Ybar = sparse.coo_matrix((self.Ybar_data[:, 2],
            (self.Ybar_data[:, 1], self.Ybar_data[:, 0])), (self.n_items, self.n_users))
        self.Ybar = self.Ybar.tocsr()
    def similarity(self):
        self.S = self.dist_func(self.Ybar.T, self.Ybar.T)
    def refresh(self):
        # Hàm gọi để chuẩn hóa và tính lại Similarity matrix
        self.normalize_Y()
        self.similarity() 
        
    def fit(self):
        self.refresh()
    def __pred(self, u, i, normalized = 1):
        """
            Dự đoán đánh giá của người dùng u lên i(normalized) 
        """
        # Step 1: Tìm tất cả các user đã rate cho i
        ids = np.where(self.Y_data[:, 1] == i)[0].astype(np.int32)
        users_rated_i = (self.Y_data[ids, 0]).astype(np.int32)
        # Step 2: Tìm độ tương tự của u với các user đã rate cho i
        sim = self.S[u, users_rated_i]
        # Step 3: Tìm k user gần tương tự nhất 
        a = np.argsort(sim)[-self.k:] 
        #  corresponding similarity levels
        nearest_s = sim[a]
        # How did each of 'near' users rated item i
        r = self.Ybar[i, users_rated_i[a]]
        if normalized:
            # add a small number, for instance, 1e-8, to avoid dividing by 0
            return (r*nearest_s)[0]/(np.abs(nearest_s).sum() + 1e-8)

        return (r*nearest_s)[0]/(np.abs(nearest_s).sum() + 1e-8) + self.mu[u]

    def recommend(self, u, normalized = 1):
        data =[]
        ids = np.where(self.Y_data[:, 0] == u)[0]
        items_rated_by_u = self.Y_data[ids, 1].tolist()              
        recommended_items = []
      
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                rating = self.__pred(u, i)
                if rating > 0:
                    data +=  [[rating, i]]
        df = pd.DataFrame(data, columns = ['rating', 'item'])
        score_series = df.sort_values(by=['rating'],ascending=False).head(10)
        score_series['item'] += 1
        #print(score_series)
        for i in score_series['item']:
            recommended_items.append(i)
        # Lưu xuống database lại
        sql = "DELETE FROM tb_cf_user WHERE user = "+str(u+1)
        
        with engine.begin() as conn:     # TRANSACTION
            conn.execute(sql)
        for i in score_series['item']:
            sql = "INSERT INTO tb_cf_user VALUES ("+str(u+1)+","+str(i)+")"
            with engine.begin() as conn:     # TRANSACTION
                conn.execute(sql)
        return recommended_items

# Lấy dữ liệu từ database  
engine = sqlalchemy.create_engine('mysql+pymysql://root:@localhost:3306/doantotnghiep?charset=utf8', encoding="utf8")
df = pd.read_sql_table('tb_rating',engine)
Y_data = df[['userID', 'quananID', 'rate']]
Y_data = Y_data.values
Y_data[:, :2] -= 1
# Sử dụng User-user CF
rs = CF(Y_data, k = 2, uuCF = 1)
rs.fit()
if __name__ == "__main__":
    quananID = int(sys.argv[1])
    rs.recommend(quananID)


	
	


