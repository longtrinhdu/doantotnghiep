<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
   protected $table = "tb_address";

        public function quanans(){
			return $this->belongsTo('App\Quanan','quananID','id');
		}
		 public function rates(){
			return $this->belongsTo('App\ratingReview','quananID','quananID');
		}
		 public function districts(){
			return $this->belongsTo('App\district','district','id');
		}
		public function provinces(){
			return $this->belongsTo('App\province','province','id');
		}
		public function streets(){
			return $this->belongsTo('App\street','street','id');
		}
		public function wards(){
			return $this->belongsTo('App\ward','ward','id');
		}
		// public function provinces(){
		// 	return $this->belongsTo('App\district_mongodb','province','_id');
		// }
		
}
