<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Quanan extends Model
{
   protected $table = 'tb_quanan';
    public function rates(){
      return $this->belongsTo('App\ratingReview','id','quananID');
    }
   public function monans()
    {
        return $this->hasMany('App\Monan','quananID', 'id');
    }
     public function address()
    {
        return $this->hasMany('App\Address','quananID', 'id');
    }
    
}
