<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Monan extends Model
{
    protected $table = 'tb_monan';
    public function quanan()
	{
	    return $this->belongsTo('App\Quanan', 'quananID', 'id');
	}
}
