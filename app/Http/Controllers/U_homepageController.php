<?php

namespace App\Http\Controllers;
use App\quanan;
use App\monan;
use App\address;
use App\province;
use App\street;
use App\district;
use App\ward;
use App\users;
use App\rating;
use App\ratingReview;
use App\tb_cb_item;
use App\tb_cf_user;
use App\tag;
use Session;
use DB;
use Illuminate\Http\Request;

class U_homepageController extends Controller
{
	
	function gettrangchu()
	{
		//$quanans = quanan::where('state','=',1)->get();
		//dd(Session('dangnhap')->id);exit();
		$quanans = quanan::where('state','=',1)->paginate(15);
		// $monans = monan::paginate(15);
		$provinces = province::all();
		$streets = street::all();
		$districts = district::all();
		$wards = ward::all();
		return view('index',compact('quanans','provinces','districts','streets','wards'));
	}

	function lienhe()
	{
		$provinces = province::all();
		$streets = street::all();
		$districts = district::all();
		$wards = ward::all();
		$tags = tag::all();
		return view('lienhe',compact('provinces','districts','streets','wards','tags'));
	}
	function chiase()
	{
		$provinces = province::all();
		$streets = street::all();
		$districts = district::all();
		$wards = ward::all();
		return view('chiase',compact('provinces','districts','streets','wards'));
	}

	function ChitietQuanan($id)
	{
		$_isNotLogin = 1;
		if(Session('dangnhap')){
		$userID = Session('dangnhap')->id - 1;
		$output2 = shell_exec("python test1.py $userID");
		$ds_goi_y2 = tb_cf_user::where('user',$userID+1)
		->join('tb_quanan','id','=','tb_cf_user.item_rec')  
		->leftJoin('tb_rating_total','quananID','=','tb_cf_user.item_rec')  
		->select('tb_cf_user.item_rec as id','name','diachi','image','average')->get();
		}
		else
		{
			$ds_goi_y2 = DB::select('call top10quanan()');
			$_isNotLogin = 0;
			//dd($ds_goi_y2);exit();
		}
		$check_output1 = tb_cb_item::where('item',$id)->first();
		if ($check_output1 === null) {
			$output1 = shell_exec("python test.py $id");
		} 
		$address = address::where('quananID',$id)->first();
		$quanan = quanan::find($id);
		$motas = DB::select('call split_mota(?,?)',array($quanan->mota,','));
		$monans = monan::Where('special',1)->get();
		
		$ratings= rating::Where('quananID',$id)
		->join('tb_users','id','=','tb_rating.userID')
		->select('userID','tb_users.name','tb_rating.rate','tb_rating.comment','tb_rating.title','tb_rating.created_at')->take(3)->get(); 
		
		$ds_goi_y1 = tb_cb_item::where('item',$id)
		->join('tb_quanan','id','=','tb_cb_item.item_rec')  
		->leftJoin('tb_rating_total','quananID','=','tb_cb_item.item_rec')  
		->select('tb_cb_item.item_rec as id','name','tb_quanan.mota','diachi','image','average')->get(); 

		return view('quanan',compact('quanan','address','monans','ratings','ds_goi_y1','ds_goi_y2','motas','_isNotLogin'));
		//dd($address->streets->name);exit();
	}

	function getTimkiem(Request $req)
	{
		if($req->street && $req->ward){
			$addresses = address::where('province',(int)$req->province)->where('district',(int)$req->district)->orWhere(function($query)use ($req)
			{
				$query->where('street', '=', (int)$req->street)
				->orWhere('ward',(int)$req->ward);
			})->get();

		}
		elseif($req->ward){
			$addresses = address::where('province',(int)$req->province)->where('district',(int)$req->district)
			->where('ward',(int)$req->ward)->get();
		}
		elseif($req->street){
			$addresses = address::where('province',(int)$req->province)->where('district',(int)$req->district)
			->where('street',(int)$req->street)->get();
		}

		elseif($req->district){
			$addresses = address::where('province',(int)$req->province)->where('district',(int)$req->district)->get();
		}
		else
			$addresses = address::where('province',(int)$req->province)->get();
		//dd($addresses[0]->quanans->image_local);exit();
		$provinces = province::all();
		$streets = street::all();
		$districts = district::all();
		$wards = ward::all();
		//dd($addresses[0]->quanans);exit();
		if($addresses->isEmpty()){
			$message = "Không tìm thấy quán chay nào ở địa chỉ này .";
			return view('timkiem',compact('message','addresses','provinces','districts','streets','wards'));
		}
		else
		{
			return view('timkiem',compact('addresses','provinces','districts','streets','wards'));
		}

	}

	function searchByName(Request $req)
	{
		
		#$quanans = DB::select('call FilterData(?)',array($req->caviarSearch));
		$quanans = quanan::where('name','like','%'.$req->caviarSearch.'%')->orWhere('diachi','like','%'.$req->caviarSearch.'%')->get();
		#dd($quanans[0]->rates);exit();
		$provinces = province::all();
		$streets = street::all();
		$districts = district::all();
		$wards = ward::all();
		if(count($quanans)==0){
			$message = "Không tìm thấy quán chay nào ở địa chỉ này .";
			return view('timkiem',compact('message','quanans','provinces','districts','streets','wards'));
		}
		else
		{
			return view('timkiem',compact('quanans','provinces','districts','streets','wards'));
		}

	}
								// Lấy dữ liệu địa chỉ đỗ ra seclect
	function getdistrict(Request $req)
	{

		$districts = district::where('_province_id',(int)$req->province_id)->get();
		Session::put('province_id',$districts[0]);
		return (response()->json($districts));
	}
	function getstreet(Request $req)
	{
		$street = street::where('_province_id','=',Session('province_id')->_province_id)
		->where('_district_id','=',(int)$req->district_id)->get();
		return (response()->json($street));
	}
	function getward(Request $req)
	{
		$ward = ward::where('_province_id','=',Session('province_id')->_province_id)
		->where('_district_id','=',(int)$req->district_id)->get();
		return (response()->json($ward));
	}
							// Lấy dữ liệu địa chỉ đỗ ra seclect

	function timkiemmap(Request $req)
	{
		$province = province::find($req->province);
		$quanans = quanan::all();
		$khuvuc=$req->province;
		//dd($quanans[0]->address[0]->province);exit();
		return view('map',compact('province','quanans','khuvuc'));
	}


	function postTimkiem(Request $req)
	{
		return $this->getTimkiem($req);
	}
	function addQuanan(Request $req)
	{
		$ids='';
		foreach($req->get('ChkID') As $c){
				$ids .= $c .',';
		}
		$mota = rtrim($ids,", ");
		//dd($mota);exit();
		$quanAn = new Quanan();
		$quanAn->name   = $req->get('tenquanan');
		$quanAn->mota   = $mota;
		$quanAn->price   = $req->get('price');
		$quanAn->worktime   = $req->get('worktime');
		$quanAn->state  = 0;

		$quanAn->save();
		Session::put('idquanan',$quanAn->id);
		return (response()->json($req->ChkID));
		//return view('lienhe');
	}
	function addQuananAddress(Request $request)
	{
		$province_m = province::where('id',(int)$request->province)->first();
		$district_m = district::where('id',(int)$request->district)->first();
		$street_m = street::where('id',(int)$request->street)->first();
		$ward_m = ward::where('id',(int)$request->ward)->first();
		if(!$province_m && $request->province){
			$province = province::where('id',$request->province)->first();
			$province_add = new province();
			$province_add->id = $province->id; 
			$province_add->name = $province->_name;
			$province_add->save();
		}
		if(!$district_m && $request->district){
			$district = district::where('id',$request->district)->first();
			$district_add=new district();
			$district_add->id = $district->id; 
			$district_add->name = $district->_name;
			$district_add->save();            
		}
		if(!$street_m && $request->street){
			$street = street::where('id',$request->street)->first();
			$street_add=new street();
			$street_add->id = $street->id; 
			$street_add->name = $street->_name;
			$street_add->save();            
		}

		if(!$ward_m && $request->ward){
			$ward = ward::where('id',$request->ward)->first();
			$ward_add=new ward();
			$ward_add->id = $ward->id; 
			$ward_add->name = $ward->_name;
			$ward_add->save();            
		}

		$quanAn_address = new Address();
		$quanAn_address->quananID   = Session('idquanan');
		$quanAn_address->province = (int)$request->get('province');
		$quanAn_address->district  = (int)$request->get('district');
		$quanAn_address->street  = (int)$request->get('street');
		$quanAn_address->ward  = (int)$request->get('ward');
		$quanAn_address->lat  = 0;
		$quanAn_address->lng  = 0;
        // dd($request->get('idquanan'));exit();
		$quanAn_address->save();
		Session::put('id_diachi',$quanAn_address->id);
	}

	function addQuananAddressMap(Request $req){
		$quanAn_address = address::find(Session('id_diachi'));
		$quanAn_address->lat  =$req->get('lat');
		$quanAn_address->lng  = $req->get('lng');
		$quanAn_address->name  = $req->get('name');
		$quanAn_address->save();
	}

	function menu()
	{
		$monans = monan::take(5)->get();
		$monans1 = monan::skip(5)->take(5)->get();
		$monans2 = monan::skip(10)->take(5)->get();
		return view('menu',compact('monans','monans1','monans2'));
	}
	function getLogin(){
		return view('dangnhap');
	}
	function getSignin(){
		return view('dangki');
	}

	function getDangxuat(){
		Session::forget('dangnhap');
		return redirect('home');
	} 
	function postLogin(Request $request){
		$email = $request['email'];
		$Salt = md5($request->password);
		$pass     = $Salt;
		$mem = Users::where('email',$email )->first();

		if($mem != null && $mem->password == $pass){
			Session::flush(); // làm mới Session
			Session::put('dangnhap',$mem);
			//dd(Session('dangnhap')->id);exit();
			return redirect('home');
		}
		else 
			return redirect()->back()->with('error','Tài khoản hoặc mật khẩu không đúng');
	}
	function danhgia(Request $req, $itemId){
		//dd($mytime);exit();
		// $newRating = new rating();
		// $newRating->userID = 1;
		// $newRating->quananID = $itemId;
		// $newRating->rate = $req->rate;
		// $newRating->comment = $req->comment;
		// $newRating->save();
		DB::select('call add_rating(?,?,?,?,?)',array(3,$itemId,$req->rate,$req->comment,$req->title));
		
		return redirect()->back();
	}
}
