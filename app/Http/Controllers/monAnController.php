<?php
namespace App\Http\Controllers;
use App\monan;
use App\quanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;

class monAnController extends Controller
{
    public function index($id)
    {
        $monans =Monan::where('quanan',$id)->get();

        Session::put('xemmonan',1);
        if ($monans->isEmpty()) {
        $quanan = quanan::all();
        return view('admin/CRUD_Dish/monan_create',compact('quanan'));
}
        else
        return view('admin/CRUD_Dish/monAn',compact('monans'));
    }
    public function index1()
    {
        $monans =Monan::Where('soLike','>=',0)->paginate(3);
         Session::put('xemmonan',0);
       // $monans=Monan::find('5bd3cdc82eca6b3254003495');
        //echo $monans->Monan->name;exit();
        return view('admin/CRUD_Dish/monAn',compact('monans'));
    }
    public function create()
    {
        $quanan = quanan::all();
        return view('admin/CRUD_Dish/monan_create',compact('quanan'));
    }
    public function store(Request $request)
    {
        $file = $request->filesTest;
        $file->move('images/monan',$file->getClientOriginalName()); 
        $monan = new Monan();
        $monan->quanan = $request->get('quanan');
        $monan->name = $request->get('name');
        $monan->moTa = $request->get('moTa');
        $monan->soLike = 1;
        $monan->price = $request->get('price');
        $monan->image = 'monan/'.$file->getClientOriginalName();
        $monan->save();
        return redirect('monan')->with('success', 'Món ăn đã được thêm.');
    }
     public function edit($id)
    {
        $monan = monan::find($id);
        return view('admin/CRUD_Dish/monan_edit',compact('monan','id'));
    }
     public function update(Request $request, $id)
    {
        $monan= monan::find($id);
        $file = $request->filesTest;
        $file->move('images/monan',$file->getClientOriginalName()); 
        $monan->name = $request->get('name');
        $monan->moTa = $request->get('mota');
        $monan->price = $request->get('price');
        $monan->image = 'monan/'.$file->getClientOriginalName();
        $monan->save();
        return redirect()->route('monan', $monan->quanan)->with('success', 'Món ăn đã được cập nhật ');
    }
    public function delete($id)
    {
        $monan = Monan::find($id);
        $monan->delete();
        return redirect('monan')->with('success','Món ăn đã được xóa');
    }
}

